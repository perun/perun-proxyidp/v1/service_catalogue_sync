# 1.0.0 (2023-09-15)


### Features

* 🎸 Initial implementation ([b9cc299](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/service_catalogue_sync/commit/b9cc299b512ed1b3ba18cec6d3c4ee288f0e43ee))
