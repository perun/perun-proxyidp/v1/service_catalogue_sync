package cz.muni.ics.serviceslistsync.common.exceptions;

public class RpIdentifierIllegalStateException extends Exception {

    public RpIdentifierIllegalStateException() {
        super();
    }

    public RpIdentifierIllegalStateException(String message) {
        super(message);
    }

    public RpIdentifierIllegalStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpIdentifierIllegalStateException(Throwable cause) {
        super(cause);
    }

    protected RpIdentifierIllegalStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
