package cz.muni.ics.serviceslistsync;

import cz.muni.ics.serviceslistsync.service.ProcessorService;
import cz.muni.ics.serviceslistsync.service.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@Slf4j
public class Application implements CommandLineRunner {

    private final ProcessorService processorService;

    @Autowired
    public Application(ProcessorService processorService) {
        this.processorService = processorService;
    }

    @Override
    public void run(String... args) {
        log.info("Started synchronization to Catalogue DB");
        Result result = processorService.process();
        log.info(
                "Finished syncing TO DB:\n" +
                        "Created {}, Updated: {}, Deleted {}, errors: {}",
                result.getCreated(),
                result.getUpdated(),
                result.getDeleted(),
                result.getErrors()
        );
    }

    public static void main(String[] args) {
        log.info("Starting application");
        new SpringApplicationBuilder(Application.class)
                .web(WebApplicationType.NONE)
                .logStartupInfo(false)
                .run(args);
        log.info("Closing application");
    }

}
