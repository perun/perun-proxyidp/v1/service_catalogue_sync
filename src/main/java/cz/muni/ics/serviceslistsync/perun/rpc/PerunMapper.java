package cz.muni.ics.serviceslistsync.perun.rpc;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.serviceslistsync.perun.model.Facility;
import cz.muni.ics.serviceslistsync.perun.model.FacilityWithAttributes;
import cz.muni.ics.serviceslistsync.perun.model.PerunAttribute;
import cz.muni.ics.serviceslistsync.perun.model.PerunAttributeValue;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * Utility class for Perun Adapter to map responses into objects.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
public class PerunMapper {

    private PerunMapper() {

    }

    /**
     * Maps JsonNode to FacilityWithAttributes model.
     * @param json Facility with attributes in JSON format from Perun to be mapped.
     * @return Mapped FacilityWithAttributes object.
     */
    public static FacilityWithAttributes mapFacilityWithAttributes(@NonNull JsonNode json) {
        if (json.isNull()) {
            return null;
        }

        Facility facility = mapFacility(json.get("facility"));
        if (facility == null) {
            return null;
        }

        Map<String, PerunAttribute> attributes = mapAttributes(json.get("attributes"));

        return new FacilityWithAttributes(facility, PerunMapper.extractAttrValues(attributes));
    }

    /**
     * Maps JsonNode to List of FacilityWithAttributes.
     * @param jsonArray array of facilities with attributes in JSON format from Perun to be mapped.
     * @return Mapped List of FacilityWithAttributes objects.
     */
    public static List<FacilityWithAttributes> mapFacilitiesWithAttributes(@NonNull JsonNode jsonArray) {
        if (jsonArray.isNull()) {
            return new ArrayList<>();
        }

        List<FacilityWithAttributes> result = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonNode facilityWithAttributesNode = jsonArray.get(i);
            FacilityWithAttributes mappedFacilityWithAttributes =
                    PerunMapper.mapFacilityWithAttributes(facilityWithAttributesNode);

            result.add(mappedFacilityWithAttributes);
        }

        return result;
    }

    /**
     * Maps JsonNode to Facility model.
     * @param json Facility in JSON format from Perun to be mapped.
     * @return Mapped Facility object.
     */
    public static Facility mapFacility(@NonNull JsonNode json) {
        if (json.isNull()) {
            return null;
        }

        Long id = json.get("id").asLong();
        String name = json.get("name").asText();
        String description = json.get("description").asText();
        String createdAt = json.get("createdAt").asText();

        return new Facility(id, name, description, createdAt);
    }

    /**
     * Maps JsonNode to PerunAttribute model.
     * @param json PerunAttribute in JSON format from Perun to be mapped.
     * @return Mapped PerunAttribute object.
     */
    public static PerunAttribute mapAttribute(@NonNull JsonNode json) {
        if (json.isNull()) {
            return null;
        }

        Long id = json.get("id").asLong();
        String friendlyName = json.get("friendlyName").asText();
        String namespace = json.get("namespace").asText();
        String description = json.get("description").asText();
        String type = json.get("type").asText();
        String displayName = json.get("displayName").asText();
        boolean writable = json.get("writable").asBoolean();
        boolean unique = json.get("unique").asBoolean();
        String entity = json.get("entity").asText();
        String baseFriendlyName = json.get("baseFriendlyName").asText();
        String friendlyNameParameter = json.get("friendlyNameParameter").asText();
        JsonNode value = json.get("value");

        return new PerunAttribute(id, friendlyName, namespace, description, type, displayName,
                writable, unique, entity, baseFriendlyName, friendlyNameParameter, value);
    }

    /**
     * Maps JsonNode to Map<String, PerunAttribute>.
     * Keys are the internal identifiers of the attributes.
     * Values are attributes corresponding to the names.
     * @param jsonArray JSON array of perunAttributes in JSON format from Perun to be mapped.
     * @return Map<String, PerunAttribute>. If attribute for identifier has not been mapped, key contains NULL as value.
     */
    public static Map<String, PerunAttribute> mapAttributes(@NonNull JsonNode jsonArray) {
        if (jsonArray.isNull()) {
            return new HashMap<>();
        }

        Map<String, PerunAttribute> mappedAttrs = new HashMap<>();

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonNode attribute = jsonArray.get(i);
            PerunAttribute mappedAttribute = PerunMapper.mapAttribute(attribute);

            if (mappedAttribute != null) {
                mappedAttrs.put(mappedAttribute.getUrn(), mappedAttribute);
            }
        }

        return mappedAttrs;
    }

    public static Map<String, PerunAttributeValue> extractAttrValues(Map<String, PerunAttribute> attributeMap) {
        if (attributeMap == null || attributeMap.isEmpty()) {
            return new HashMap<>();
        }

        Map<String, PerunAttributeValue> resultMap = new LinkedHashMap<>();
        attributeMap.forEach((identifier, attr) -> resultMap.put(identifier, attr == null ?
            null : attr.toPerunAttributeValue())
        );

        return resultMap;
    }

}

