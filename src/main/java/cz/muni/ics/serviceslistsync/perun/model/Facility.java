package cz.muni.ics.serviceslistsync.perun.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;

/**
 * Facility object model.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Facility {

    @NonNull private Long id;
    @NotBlank private String name;
    @NonNull private String description = "";
    @NotBlank private String createdAt;

    public Facility(Long id, String name, String description, String createdAt) {
        this.setId(id);
        this.setName(name);
        this.setDescription(description);
        this.setCreatedAt(createdAt);
    }

    public void setName(@NonNull String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("name cannot be empty");
        }

        this.name = name;
    }

}
