package cz.muni.ics.serviceslistsync.perun.exception;

/**
 * Represents an error when obtaining connection to Perun interface (RPC).
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
public class PerunConnectionException extends Exception {

    public PerunConnectionException() {
        super();
    }

    public PerunConnectionException(String s) {
        super(s);
    }

    public PerunConnectionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PerunConnectionException(Throwable throwable) {
        super("Error when contacting Perun RPC", throwable);
    }

    protected PerunConnectionException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

}

