package cz.muni.ics.serviceslistsync.service;

import cz.muni.ics.serviceslistsync.service.sub.ServiceSynchronizer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ProcessorService {

    private final ServiceSynchronizer serviceSynchronizer;

    @Autowired
    public ProcessorService(@NonNull ServiceSynchronizer serviceSynchronizer) {
        this.serviceSynchronizer = serviceSynchronizer;
    }

    public Result process() {
        log.info("Started synchronization to DB");
        return serviceSynchronizer.process();
    }

}
