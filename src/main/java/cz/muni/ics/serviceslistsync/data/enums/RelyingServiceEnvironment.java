package cz.muni.ics.serviceslistsync.data.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration of RP environments
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@AllArgsConstructor
@Getter
public enum RelyingServiceEnvironment {

    TEST("TEST"),
    PRODUCTION("PRODUCTION"),
    UNKNOWN("UNKNOWN");

    private final String value;

    private static final Map<String, RelyingServiceEnvironment> lookup = new HashMap<>();

    static {
        for (RelyingServiceEnvironment status : EnumSet.allOf(RelyingServiceEnvironment.class)) {
            lookup.put(status.value, status);
        }
    }

    public static RelyingServiceEnvironment resolve(String value) {
        return lookup.getOrDefault(value, UNKNOWN);
    }

}
