package cz.muni.ics.serviceslistsync.data;

import cz.muni.ics.serviceslistsync.data.model.RelyingServiceDTO;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
import java.util.Set;

/**
 * Repository for the RelyingServiceDTO
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
public interface RelyingServicesRepository extends MongoRepository<RelyingServiceDTO, Long> {

    Optional<RelyingServiceDTO> findByRpIdentifier(String rpIdentifier);

    void deleteAllByRpIdentifierIn(Set<String> rpIdentifiers);

}
