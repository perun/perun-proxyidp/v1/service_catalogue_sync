package cz.muni.ics.serviceslistsync.data.helper;

import cz.muni.ics.serviceslistsync.data.model.DatabaseSequenceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

/**
 * Generator for mongo sequences based on numeric IDs.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@Component
public class SequenceGenerator {

    private final MongoOperations mongoOperations;

    @Autowired
    public SequenceGenerator(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public long generateSequence(String seqName) {
        DatabaseSequenceDTO counter = mongoOperations.findAndModify(query(where("_id").is(seqName)),
                new Update().inc("seq",1), options().returnNew(true).upsert(true),
                DatabaseSequenceDTO.class);
        return !Objects.isNull(counter) ? counter.getSeq() : 1;
    }

}
